<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="FastFriends" version="1.7" date="10/21/2008">
		<Author name="shammus" email="" />
		<Description text="Allows you to keep track of friends across characters" />
		<Dependencies />
		<Files>
			<File name="FastFriends.lua" />
		</Files>
		<OnInitialize>
			<CallFunction name="FastFriends.OnInitialize" />
		</OnInitialize>
		<OnUpdate />
		<OnShutdown />
		<SavedVariables>
			<SavedVariable name="FastFriendsDB" />
		</SavedVariables>
	</UiMod>
</ModuleFile>