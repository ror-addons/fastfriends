-- DECLARE ADDON ---------------------------------------------------------------
FastFriends = {
	tag = L"[FastFriends] (v1.7): ",
	hasLoaded = false,
	serverName = WStringToString(GameData.Account.ServerName),
	myName = "",
	actionQueue = {},
	isDebug = true
}
--------------------------------------------------------------------------------


-- EVENT HANDLERS --------------------------------------------------------------
function FastFriends.OnInitialize()
	FastFriends.Debug("FastFriends.OnInitialize()")

	-- reset loaded flag for /reloadui and zoning
	FastFriends.hasLoaded = false

	-- load player name
	FastFriends.myName = FastFriends.TrimName(GameData.Player.name)

	-- initialize friends database and server table
	if FastFriendsDB == nil then FastFriendsDB = {} end
	if FastFriendsDB[FastFriends.serverName] == nil then
		FastFriendsDB[FastFriends.serverName] = {}
		FastFriendsDB[FastFriends.serverName].friend = {}
		FastFriendsDB[FastFriends.serverName].ignore = {}
		FastFriendsDB[FastFriends.serverName].friend._deleted = {}
		FastFriendsDB[FastFriends.serverName].ignore._deleted = {}
	end

	-- register event handlers
	RegisterEventHandler( SystemData.Events.LOADING_END, "FastFriends.LoadingEnd" )

	-- print loaded message
	FastFriends.Print(L"Loaded")
end


function FastFriends.LoadingEnd()
	FastFriends.Debug("FastFriends.LoadingEnd()")

	if not FastFriends.hasLoaded then
		-- reconcile friends
		local localFriends = FastFriends.GetLocalNameList("friend")
		localFriends = FastFriends.ReconcileDeletes(localFriends, FastFriendsDB[FastFriends.serverName].friend._deleted, "friend")
		FastFriends.Reconcile(localFriends, FastFriendsDB[FastFriends.serverName].friend, "friend")

		-- reconcile ignores
		local localIgnores = FastFriends.GetLocalNameList("ignore")
		localIgnores = FastFriends.ReconcileDeletes(localIgnores, FastFriendsDB[FastFriends.serverName].ignore._deleted, "ignore")
		FastFriends.Reconcile(localIgnores, FastFriendsDB[FastFriends.serverName].ignore, "ignore")

		-- register social update handlers, now that we are ready for them
		RegisterEventHandler( SystemData.Events.SOCIAL_FRIENDS_UPDATED, "FastFriends.Update" )
		RegisterEventHandler( SystemData.Events.SOCIAL_IGNORE_UPDATED, "FastFriends.Update" )

		-- fire off first action, if present
		if #FastFriends.actionQueue > 0 then
			FastFriends.AddOrRemoveCharacter(FastFriends.actionQueue[1].name, FastFriends.actionQueue[1].type)
		end

		FastFriends.hasLoaded = true
	end
end


function FastFriends.Update()
	FastFriends.Debug("FastFriends.Update(): #FastFriends.actionQueue="..tostring(#FastFriends.actionQueue))

	if #FastFriends.actionQueue == 0 then
		-- determine if an add or delete occurred
		local localFriends = FastFriends.GetLocalNameList("friend")
		local localIgnores = FastFriends.GetLocalNameList("ignore")

		local localFriendCount = FastFriends.GetTableSize(localFriends)
		local localIgnoreCount = FastFriends.GetTableSize(localIgnores)
		local globalFriendCount = FastFriends.GetTableSize(FastFriendsDB[FastFriends.serverName].friend)
		local globalIgnoreCount = FastFriends.GetTableSize(FastFriendsDB[FastFriends.serverName].ignore)

		FastFriends.Debug("localFriendCount: "..tostring(localFriendCount).." / globalFriends: "..tostring(globalFriendCount))
		FastFriends.Debug("localIgnoreCount: "..tostring(localIgnoreCount).." / globalIgnores: "..tostring(globalIgnoreCount))

		if localFriendCount > globalFriendCount then
			FastFriends.Reconcile(localFriends, FastFriendsDB[FastFriends.serverName].friend, "friend")
		elseif localIgnoreCount > globalIgnoreCount then
			FastFriends.Reconcile(localIgnores, FastFriendsDB[FastFriends.serverName].ignore, "ignore")
		elseif localFriendCount < globalFriendCount then
			FastFriends.AddToDeletedList(localFriends, "friend")
		elseif localIgnoreCount < globalIgnoreCount then
			FastFriends.AddToDeletedList(localIgnores, "ignore")
		end
	else
		-- remove first action from the table
		table.remove(FastFriends.actionQueue, 1)
	end

	-- fire off the next action, if present
	if #FastFriends.actionQueue > 0 then
		FastFriends.AddOrRemoveCharacter(FastFriends.actionQueue[1].name, FastFriends.actionQueue[1].type)
	end
end
--------------------------------------------------------------------------------


-- CUSTOM METHODS --------------------------------------------------------------
function FastFriends.ReconcileDeletes(localList, deleteList, reconcileType)
	FastFriends.Debug("FastFriends.ReconcileDeletes: "..reconcileType)

	-- remove deleted characters from local list
	for name,_ in pairs(localList) do
		if deleteList[name] ~= nil then
			FastFriends.Print(towstring(" - removing '"..name.."' from local "..reconcileType.." list"))
			FastFriends.AddAction(name, reconcileType)
			localList[name] = nil
		end
	end

	return localList
end


function FastFriends.Reconcile(localList, globalList, reconcileType)
	FastFriends.Debug("Reconcile(): "..reconcileType)

	-- add local characters to global list
	for name,_ in pairs(localList) do
		if globalList[name] == nil then
			globalList[name] = true
			globalList._deleted[name] = nil
			FastFriends.Print(towstring(" + adding '"..name.."' to global "..reconcileType.." list"))
		end
	end

	-- add global characters to local list
	for name,_ in pairs(globalList) do
		if localList[name] == nil and name ~= "_deleted" and name ~= FastFriends.myName then
			FastFriends.Print(towstring(" + adding '"..name.."' to local "..reconcileType.." list"))
			FastFriends.AddAction(name, reconcileType)
		end
	end
end


function FastFriends.AddAction(name, action)
	FastFriends.Debug("FastFriends.AddAction(): "..action.." - "..name)

	local index = #FastFriends.actionQueue + 1

	FastFriends.actionQueue[index] = {}
	FastFriends.actionQueue[index].name = name
	FastFriends.actionQueue[index].type = action
end


function FastFriends.AddOrRemoveCharacter(name, slashCommand)
	FastFriends.Debug("FastFriends.AddOrRemoveCharacter(): "..slashCommand.." - "..name)

	-- add character name to add list
	-- api can't add more than one character at a time, so they need to be spaced out.
	SystemData.UserInput.ChatText = towstring("/"..slashCommand.." "..name)
	BroadcastEvent( SystemData.Events.SEND_CHAT_TEXT )
end


function FastFriends.AddToDeletedList(localList, listType)
	FastFriends.Debug("FastFriends.DeleteCharacters(): "..listType)

	-- determine which characters are missing
	for name,_ in pairs(FastFriendsDB[FastFriends.serverName][listType]) do
		if localList[name] == nil and name ~= "_deleted" and name ~= FastFriends.myName then
			FastFriendsDB[FastFriends.serverName][listType][name] = nil
			FastFriendsDB[FastFriends.serverName][listType]._deleted[name] = true
			FastFriends.Print(towstring(" - removed '"..name.."' from global "..listType.." list"))
		end
	end
end


function FastFriends.GetLocalNameList(listType)
	local localList = {}
	local serverList = {}

	if listType == "friend" then
		serverList = GetFriendsList()
	elseif listType == "ignore" then
		serverList = GetIgnoreList()
	end

	for index, character in pairs(serverList) do
	    localList[FastFriends.TrimName(character.name)] = true
	end

	return localList
end
--------------------------------------------------------------------------------


-- UTILITY METHODS -------------------------------------------------------------
function FastFriends.Print(text)
	TextLogAddEntry("Chat", 0, FastFriends.tag..text)
end

function FastFriends.Debug(object)
	if FastFriends.isDebug then
	    if object ~= nil then
			d(object)
		else
		    d("FastFriends.Debug: nil object")
		end
	end
end

function FastFriends.TrimName(text)
	-- remove the localization ^M or ^F from the string
	local len = string.len(WStringToString(text))
	local name = string.sub(WStringToString(text), 1, len-2)
	return name
end

function FastFriends.GetTableSize(table)
	local size = 0

	for name,_ in pairs(table) do
		if name ~= "_deleted" and name ~= FastFriends.myName then
			size = size + 1
		end
	end

	return size
end
--------------------------------------------------------------------------------
